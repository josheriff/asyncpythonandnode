import aiofiles
import json
from aiohttp import web
import asyncio
import uvloop


def main():
    async def handle(request):
        return await food_json()

    async def food_json_library():
        async with aiofiles.open('data/food.json', 'r') as f:
            response = await f.read()
            response = json.loads(response)
            return web.json_response(response)

    async def food_json():
        with open('data/food.json', 'r') as f:
            response = json.load(f)
            return web.json_response(response)

    app = web.Application()
    app.add_routes([web.get('/', handle)])
    web.run_app(app)


asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
asyncio.run(main())
# main()
