const fs = require('fs')
const { promisify } = require('util')
const readFileAsync = promisify(fs.readFile)
const express = require('express');
const app = express();


app.get('/', run);

async function run (req, res) {
  const response = await readFileAsync('data/food.json')
  res.send(JSON.parse(response))
}

app.listen(8080, function () {
    console.log('Example app listening on port 8080!');
});
