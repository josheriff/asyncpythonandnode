## Modelos

## asincronía

![El modelo asincrono](./images/EventLoop.png)

## concurrencia y pararelismo

![Ejemplo básico](./images/concurrence.png)

Extraido de [este articulo](https://medium.com/@deepshig/concurrency-vs-parallelism-4a99abe9efb8) 

